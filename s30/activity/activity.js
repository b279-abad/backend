
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", fruitsOnSale: {$sum: 1}}},
  {$project: {_id: 0}}
]);


db.fruits.aggregate([
  {$match: {stock: {$gte: 20}}},
  {$group: {_id: "$supplier_id", enoughStock: {$sum: 1}}},
  {$project: {_id: 0}}
]);


db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
  {$sort: {total: 1}}
]);

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", max_price: {$min: "$price"}}},
  {$sort: {total: 1}}
]);