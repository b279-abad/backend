// 1. What directive is used by Node.js in loading the modules it needs?
const http = require("http");
const port = 3000;


// 2. What Node.js module contains a method for server creation?

// http module


// 3. What is the method of the http object responsible for creating a server using Node.js?
const server = http.createServer((request, response) => {

// 4. What method of the response object allows us to set status codes and content types?
	
if(request.url == "/login"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Welcome to the login page.");
	}else{
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("I'm sorry the page you are looking for cannot be found.");
		
	}
});

// 5. Where will console.log() output its contents when run in Node.js?

// gitbash/terminal


// 6. What property of the request object contains the address's endpoint?
server.listen(port);
console.log(`Server now accessible at localhost:${port}`);
