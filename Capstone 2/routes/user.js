const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.js');
const auth = require("../auth.js")

// Route for checking if email exist
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for registering a user.
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for logging in a user.
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile(userData.id).then(resultFromController => res.send(resultFromController));
});

// Create Order
router.post("/checkout", auth.verify, (req, res,) => {
    let data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        userId : auth.decode(req.headers.authorization).id,
        product : req.body
    }
        userController.checkout(data).then(resultFromController => res.send(resultFromController));
});





// router.put("/checkout", (req, res) => {
//     let data = {
//         product: req.body,
//         userId: auth.decode(req.headers.authorization).id,
//         isAdmin: auth.decode(req.headers.authorization).isAdmin
//     }

//     userController.createOrder(data).then(resultFromController => res.send(resultFromController))
// })

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
