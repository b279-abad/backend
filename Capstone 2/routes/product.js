const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController.js');
const auth = require('../auth.js');

// Route for creating a product
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Get all products
router.get("/all", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.getAllProducts(isAdmin).then(resultFromController => res.send(resultFromController));
})

// Get all "ACTIVE" products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Retrieve a specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

})

// Update a specific product
router.put("/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

})

// Archive a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
	
});


// Allows us to export the "router" object that will be accessed in our "index.js" file

module.exports = router;
