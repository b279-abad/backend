const Product = require('../models/Product.js');

// Create a new product
module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("Sorry! Only admin can do this action.");

	return message.then((value) => {
		return value
	})
}

// Retrieve all products function
module.exports.getAllProducts = (isAdmin) => {
	if(isAdmin){

	return Product.find({}).then(result => {
		return result;
	})
}else{
	return Promise.resolve("Sorry! Only admin can do this action.")
}
}

// Retrieve all active products function
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

// Retreive a specific product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
}

// Update a product
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return "Product Updated!";
			}
		})
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("Sorry! Only admin can do this action.");

    return message.then((value) => {
        return value
    })

 }

// Archive a product
module.exports.archiveProduct = (reqParams, isAdmin) => {
    console.log(isAdmin);

        let updateActiveField = {
        isActive : false
        };

        return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

        	// Product not archived
            if(error){
                return false;
            // Product archived successfully
            }else{
                return "Product Archived!";
            }
        });
    }

    // If the user is not admin, then return this message as a promise to avoid errors
    let message = Promise.resolve("Sorry! Only admin can do this action.");

    return message.then((value) => {
        return value

    });

