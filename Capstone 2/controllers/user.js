const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require('../models/Product.js');

// Check if the email already exist

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => { 
	if (result.length > 0){
		return true;
	} else {
		return false;
	};

	});

};

// User registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 is the value provided as the number of salt rounds.
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});
};

// User authetication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

// Retrieve user details
module.exports.getProfile = (userData) => {

	return User.findById(userData).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return result;
		};
	});
};

// Create order
module.exports.checkout = async (data, reqBody, isAdmin) => {
    console.log(data.isAdmin);

    if(data.isAdmin){
            return "Admin not allowed to order!"
    }else{
        let isUserUpdated = await User.findById(data.userId).then(user => {
            return Product.findById(data.product.productId).then(result =>{
                console.log(result.name)
            let newOrder = {
                products : [{
                    productId : data.product.productId,
                    productName : result.name,
                    quantity : data.product.quantity
                }],
                totalAmount : result.price * data.product.quantity
            }
            user.orderedProduct.push(newOrder);


        console.log(data.product.productId);

        return user.save().then((user, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })
            })

    })


    let isProductUpdated = await Product.findById(data.product.productId).then(product => {

        product.userOrders.push({userId: data.userId})

        return product.save().then((product, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })

        })


    if(isUserUpdated && isProductUpdated){
        return "Order Successfully Placed!";
    }else{
        return "Something went wrong with your request. Please try again later!";
    }
    }
}








// module.exports.createOrder = async (data) => {
//     if(data.isAdmin){
//         return false
//     } else {
//         let isUserUpdated = await User.findOne({_id: data.userId}).then(userRes => {
//             Product.findById({_id: data.product.productId}).then(prodRes => {
//                  userRes.orderedProduct.push({
//                     products: {
//                         productId: data.product.productId,
//                         productName: prodRes.name,
//                         quantity: data.product.quantity
//                     },
//                     totalAmount: prodRes.price * data.product.quantity,
//                 })

//                 return userRes.save().then((res, err) => {
//                     if(err) {
//                         return false
//                     } else {
//                         return true
//                     }
//                 })
//             })
//         })

//         let isOrderedProduct =  await Product.findOne({_id: data.product.productId}).then(res => {
//             res.userOrders.push({
//                 userId: data.userId
//             })

//             return res.save().then((res, err) => {
//                 if(err){
//                     return false
//                 } else {
//                     return true
//                 }
//             })
//         })

//         if(isUserUpdated && isOrderedProduct){
//             return Promise.resolve(true)
//         } else {
//             return Promise.resolve(false)
//         }
//     }
// }

   
        //     return Product.findById(data.product.productId).then(userRes => {
        //     	console.log(userRes);
        //         userRes.orderedProduct.push({
        //             product: [{
        //                 productId: data.product.productId,
        //                 productName: prodRes.productName,
        //                 quantity: data.product.quantity
        //             }],
        //             totalAmmount: prodRes.price * data.product.quantity
        //         })
        //         return userRes.save().then((res, err) => {
        //             if(err) {
        //                 return false;
        //             } else {
        //                 return true;
        //             }
        //         })
        //     })
        // }

        // if(isUserUpdated){
        //     return Promise.resolve(true)
        // } else {
        //     return Promise.resolve(false)
        // }